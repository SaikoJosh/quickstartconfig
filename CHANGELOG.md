# Changelog

### **`v1.3.5`** / 2024-02-05

- `Improve:` No need to specify public member accessor keyword.
- `Improve:` Don't auto-delete unused import statement.

### **`v1.3.4`** / 2024-02-04

- `Improve:` Remove buggy rule.

### **`v1.3.3`** / 2024-02-04

- `Improve:` Tweak ESLint rules.

### **`v1.3.2`** / 2024-02-04

- `New:` Add prettier plugins.
- `Improve:` Update TypeScript version.

### **`v1.2.1`** / 2023-07-29

- `Improve:` Update TS config.

### **`v1.2.0`** / 2023-07-29

- `Improve:` Update dependencies.

### **`v1.0.6`** / 2022-09-30

- `New:` Ignore *.d.ts files from Jest coverage.

### **`v1.0.0`** / 2022-05-02

- `New:` Initial release.
