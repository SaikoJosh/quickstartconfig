const c8 = require(`./c8.json`);
const eslint = require(`./eslint.cjs`);
const eslintReact = require(`./eslintReact.cjs`);
const eslintReactNative = require(`./eslintReactNative.cjs`);
const jest = require(`./jest.cjs`);
const prettier = require(`./prettier.cjs`);
const tsconfig = require(`./tsconfig.json`);

module.exports = {
	c8,
	eslint,
	eslintReact,
	eslintReactNative,
	jest,
	prettier,
	tsconfig,
};
