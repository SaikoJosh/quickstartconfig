module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2020,
		project: './tsconfig.json',
		ecmaFeatures: {},
	},
	plugins: ['@typescript-eslint', 'eslint-plugin-tsdoc', 'eslint-plugin-import', 'eslint-comments', 'jest', 'prettier'],
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:eslint-comments/recommended',
		'plugin:jest/recommended',
		'prettier',
	],
	settings: {
		jest: {
			version: 'detect',
		},
	},
	env: {
		'jest/globals': true,
	},
	rules: {
		// Core rules.
		'no-return-await': 0, // Must be off for "@typescript-eslint/return-await" rule to work.
		'quotes': 0, // Must be off for "@typescript-eslint/quotes" rule to work.
		'no-shadow': 0, // Must be off for "@typescript-eslint/no-shadow" rule to work.
		'no-redeclare': 0, // Must be off for "@typescript-eslint/no-redeclare" rule to work.
		'no-loop-func': 0, // Must be off for "@typescript-eslint/no-loop-func" rule to work.
		'dot-notation': 0, // Must be off for "@typescript-eslint/dot-notation" rule to work.
		'default-param-last': 0, // Must be off for "@typescript-eslint/default-param-last" rule to work.
		'no-console': 2,
		'eol-last': 2,

		// PLUGIN: TypesScript.
		'@typescript-eslint/adjacent-overload-signatures': 2,
		'@typescript-eslint/no-unused-vars': 2,
		'@typescript-eslint/array-type': [2, { default: 'array' }],
		'@typescript-eslint/no-confusing-non-null-assertion': 2,
		'@typescript-eslint/no-base-to-string': 2,
		'@typescript-eslint/method-signature-style': 2,
		'@typescript-eslint/consistent-type-assertions': [
			2,
			{
				assertionStyle: 'as',
				objectLiteralTypeAssertions: 'allow-as-parameter',
			},
		],
		'@typescript-eslint/consistent-type-definitions': [2, 'interface'],
		'@typescript-eslint/consistent-type-imports': [2, { prefer: 'type-imports', disallowTypeAnnotations: true }],
		'@typescript-eslint/explicit-function-return-type': [
			2,
			{
				allowConciseArrowFunctionExpressionsStartingWithVoid: true,
			},
		],
		'@typescript-eslint/explicit-member-accessibility': [2, { accessibility: 'no-public' }],
		'@typescript-eslint/member-delimiter-style': [
			2,
			{
				multiline: {
					delimiter: 'semi',
					requireLast: true,
				},
				singleline: {
					delimiter: 'semi',
					requireLast: true,
				},
			},
		],
		'@typescript-eslint/no-extraneous-class': 2,
		'@typescript-eslint/no-dynamic-delete': 2,
		'@typescript-eslint/no-invalid-void-type': 2,
		'@typescript-eslint/no-inferrable-types': 0,
		'@typescript-eslint/no-require-imports': 2,
		'@typescript-eslint/no-throw-literal': 2,
		'@typescript-eslint/no-unnecessary-boolean-literal-compare': 2,
		'@typescript-eslint/no-unnecessary-condition': 2,
		'@typescript-eslint/prefer-enum-initializers': 0,
		'@typescript-eslint/prefer-for-of': 2,
		'@typescript-eslint/prefer-literal-enum-member': 2,
		'@typescript-eslint/prefer-nullish-coalescing': 1,
		'@typescript-eslint/prefer-optional-chain': 2,
		'@typescript-eslint/prefer-readonly': 2,
		'@typescript-eslint/prefer-reduce-type-parameter': 2,
		'@typescript-eslint/promise-function-async': 2,
		'@typescript-eslint/require-await': 1,
		'@typescript-eslint/no-floating-promises': 2,
		'@typescript-eslint/require-array-sort-compare': 2,
		'@typescript-eslint/restrict-plus-operands': 2,
		'@typescript-eslint/switch-exhaustiveness-check': 2,
		'@typescript-eslint/typedef': [
			2,
			{
				memberVariableDeclaration: true,
				parameter: true,
				propertyDeclaration: true,
			},
		],
		'@typescript-eslint/naming-convention': [
			2,
			{
				selector: 'default',
				format: ['camelCase'],
			},
			{
				selector: 'variable',
				modifiers: ['const'],
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
			},
			{
				selector: 'property',
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
				filter: {
					regex: '[- ]', // Enables quoted properties.
					match: false,
				},
			},
			{
				selector: 'objectLiteralProperty',
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
			},
			{
				selector: 'typeLike',
				format: ['PascalCase'],
			},
			{
				selector: 'enum',
				format: ['UPPER_CASE'],
			},
			{
				selector: 'enumMember',
				format: ['UPPER_CASE'],
			},
			{
				selector: 'import', // Default or namespace imports, NOT named imports.
				format: ['camelCase', 'PascalCase'],
			}
		],
		'@typescript-eslint/unified-signatures': 2,
		'@typescript-eslint/return-await': 2,
		'@typescript-eslint/quotes': [2, 'backtick'],
		'@typescript-eslint/no-shadow': 2,
		'@typescript-eslint/no-redeclare': 2,
		'@typescript-eslint/no-loop-func': 2,
		'@typescript-eslint/dot-notation': 2,
		'@typescript-eslint/default-param-last': 2,

		// PLUGIN: TS Doc.
		'tsdoc/syntax': 1,

		// PLUGIN: Import.
		'import/no-absolute-path': 2,
		'import/no-internal-modules': [2, { allow: ['next/*', '*/*.css', '*/*.scss', '*/*.sass', '*/*.svg', '*/*.svgz'] }],
		'import/no-cycle': [2, { maxDepth: 20, ignoreExternal: true }],
		'import/no-self-import': 2,
		'import/no-deprecated': 1,
		'import/no-extraneous-dependencies': 2,
		'import/no-mutable-exports': 2,
		'import/newline-after-import': 2,
		'import/no-anonymous-default-export': 2,
		'import/first': 2,
		'import/order': 0, // Must be off so it does not conflict with Prettier plugin "prettier-plugin-organize-imports".

		// PLUGIN: ESLint Comments.
		'eslint-comments/require-description': 1,

		// PLUGIN: Jest.
		'jest/consistent-test-it': [2, { fn: 'test', withinDescribe: 'test' }],
		'jest/max-nested-describe': [2, { max: 5 }],
		'jest/no-conditional-expect': 1,
		'jest/no-done-callback': 0, // Incorrectly complains about cucumber parameters.
		'jest/no-duplicate-hooks': 2,
		'jest/prefer-hooks-on-top': 2,
		'jest/prefer-todo': 1,
		'jest/no-standalone-expect': [2, { additionalTestBlockFunctions: ['Then'] }],

		// PLUGIN: Prettier.
		'prettier/prettier': 1,
	},
};
