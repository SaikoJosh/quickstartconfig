const IGNORE_PATHS = [`<rootDir>/node_modules`, `<rootDir>/build`, `<rootDir>/dist`];

module.exports = {
	errorOnDeprecated: true,
	testPathIgnorePatterns: [...IGNORE_PATHS],
	coveragePathIgnorePatterns: [...IGNORE_PATHS],
	testTimeout: 5_000,
	verbose: false,
	testMatch: [`**/*.spec.[jt]s?(x)`],
	coverageReporters: [`text`, `text-summary`],
	coverageThreshold: {
		global: {
			branches: 100,
			functions: 100,
			lines: 100,
			statements: 100,
		},
	},
	collectCoverageFrom: [
		`**/*.{ts,tsx,js,jsx}`,
		`!**/.eslintrc.js`,
		`!**/.prettierrc.js`,
		`!**/node_modules/**`,
		`!**/coverage/**`,
		`!**/build/**`,
		`!**/dist/**`,
		`!**/test/**`,
		`!**/tests/**`,
		`!**/*.d.ts`,
	],
};
