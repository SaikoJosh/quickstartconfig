/**
 * Builds on the base eslint config file.
 */

const baseConfig = require(`./eslint.cjs`);

module.exports = {
	...baseConfig,
	parserOptions: {
		...baseConfig.parserOptions,
		ecmaFeatures: {
			...baseConfig.parserOptions.ecmaFeatures,
			jsx: true,
		},
	},
	plugins: [
		...baseConfig.plugins,
		'react',
		'react-hooks',
	],
	extends: [
		...baseConfig.extends,
		'plugin:react/recommended',
		'plugin:react-hooks/recommended',
	],
	settings: {
		...baseConfig.settings,
		react: {
			version: `detect`,
		},
	},
	rules: {
		...baseConfig.rules,

		// PLUGIN: TypeScript.
		'@typescript-eslint/naming-convention': [
			...baseConfig.rules['@typescript-eslint/naming-convention'],
			{
				selector: 'variable',
				modifiers: ['const'],
				types: ['function'],
				format: ['PascalCase', 'camelCase'],
			},
			{
				selector: 'parameter',
				modifiers: ['destructured'],
				format: ['PascalCase', 'camelCase'],
			},
			{
				selector: 'function',
				format: ['camelCase', 'PascalCase'],
			},
		],
	},
};
