module.exports = {
	plugins: [`prettier-plugin-organize-imports`],
	organizeImportsSkipDestructiveCodeActions: true,
	arrowParens: 'avoid',
	bracketSpacing: true,
	endOfLine: 'lf',
	printWidth: 120,
	quoteProps: 'consistent',
	semi: true,
	singleQuote: true,
	tabWidth: 2,
	trailingComma: 'all',
	useTabs: true,
};
