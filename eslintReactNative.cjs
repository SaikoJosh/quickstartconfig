/**
 * Builds on the 'react' eslint config file.
 */

const baseConfig = require(`./eslintReact.cjs`);

module.exports = {
	...baseConfig,
	plugins: [
		...baseConfig.plugins,
		'react-native',
	],
	env: {
		...baseConfig.env,
		'react-native/react-native': true,
	},
	settings: {
		...baseConfig.settings,
		'import/ignore': ['node_modules/react-native/index\\.js$'], // Workaround for: https://github.com/facebook/react-native/issues/28549#issuecomment-657249702
	},
	rules: {
		...baseConfig.rules,

		// PLUGIN: React Native.
		'react-native/no-unused-styles': 2,
		'react-native/sort-styles': 0,
		'react-native/split-platform-components': 2,
		'react-native/no-inline-styles': 2,
		'react-native/no-color-literals': 2,
		'react-native/no-raw-text': 0, // BUG: Set to '2' once bug is fixed (broken as of 3.11.0) - https://github.com/Intellicode/eslint-plugin-react-native/issues/270
		'react-native/no-single-element-style-arrays': 2,

		// PLUGIN: Import.
		'import/no-extraneous-dependencies': 0, // Doesn't seem to play nice with React Native.
	},
};
